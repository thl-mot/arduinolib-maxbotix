/*
 * MaxBotix.h
 *
 *  Created on: 22.06.2018
 *      Author: thomas
 */

#ifndef MAXBOTIX_H_
#define MAXBOTIX_H_

class MaxBotix {
private:
	// 0= wait for "R", 1,2,3,4= data
	int    state=0;
	unsigned int distanceMM=0;
	int buffer;
public:
	MaxBotix();
	~MaxBotix();

	bool handle(char c);
	double getDistance();
	unsigned int getMM();
	void reset();
};

#endif /* MAXBOTIX_H_ */
