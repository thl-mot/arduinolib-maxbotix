/*
 * MaxBotix.cpp
 *
 *  Created on: 22.06.2018
 *      Author: thomas
 */

#include "MaxBotix.h"

MaxBotix::MaxBotix() {
	buffer = 0;
}

MaxBotix::~MaxBotix() {
}

bool MaxBotix::handle(char c) {
	if (c == 'R') { // wait for 'R'
		state = 1; // now wait for first number
		buffer = 0;
		// distanceMM = 0;
	} else if (state >= 1 && state <= 4) {  // wait for 4 numbers
		buffer = buffer * 10 + (c - '0');
		state++;
	}
	if (state == 5) {
		distanceMM = buffer;
		state = 0;
		return true;
	}
	return false;
}

double MaxBotix::getDistance() {
	return (double) distanceMM / (double) 1000;
}

unsigned int MaxBotix::getMM() {
	return distanceMM;
}

void MaxBotix::reset() {
	state=0;
	buffer=0;
}
